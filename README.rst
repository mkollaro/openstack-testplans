====================
OpenStack Test Plans
====================

These documents are a collection of test plans for new features, to be linked
in the blueprints.

The reasoning behind this is that formatting in blueprints is very basic and
it's easier to use the ReStructuredText format and Git. Test plans
are pretty much pseudo code and using programming tools is more comfortable,
makes for easier reviews and it makes developers more likely to actually write
the test plans.
