===============================================
Test plan for the Glance multi-location feature
===============================================

:release: Havana
:blueprint: `multiple-image-locations
    <https://blueprints.launchpad.net/glance/+spec/multiple-image-locations>`_

The purpose of this feature is to make Glance just a registry of the image
locations, so that Nova could look at the paths and decide which one to use. 

Some things you'd expect from this feature are not implemented yet
(and some of them maybe never will be, depending on the direction this
takes).  For now, Glance just serves the first location it has when asked
for the image, it doesn't try the second location if it failed. Also, when
a second location for an image is added, it doesn't really check if the
image is the same, you have to ensure that yourself. By the time this tests
actually get run, this could have changed, so ask some developers if it got 
implemented by then. In that case, a few other tests
should be added.

Nova accessing the images using the locations directly is another
story (not tested here), look at the new 
`download modules in Nova
<https://tropicaldevel.wordpress.com/2013/08/07/download-modules-in-nova/>`_.

Setup
===============================================================================

* install Glance and set up multiple backends, I'll be assuming Swift and local
  FS
* add the following line to ``glance-api.conf``::

        show_multiple_locations = True

* check that you have Glance API v2 enabled
  (try `this configuration
  <https://blueprints.launchpad.net/glance/*spec/registry-db-driver>`_
  if you have problems)
* set ``$AUTH`` to the keystone token, you can get it using::

        $ keystone token-get
* **Note:** you will probably need to do the location adding as an admin user
  (it's possible that it got enabled for normal users, ask some devel)
* **Note:** all Glance commands should use API version 2, unless stated
  otherwise. Some commands are not available yet in v2 (e.g. image-create),
  temporary use v1 if you encounter this.

Test #1 (smoke) - check if the default location is shown
===============================================================================
* upload an image to Glance::

        $ glance image-create ...
* use the Glance client to 'show' an image with the v2 protocol::

        $ glance --os-image-api-version 2 image-show <image_id>
* see 1 location listed in the output
* use curl to show the image::

        $ curl -i -X HEAD
         -H 'Content-Type: application/openstack-images-v2.1-json-patch'
         -H "X-Auth-Token: $AUTH"
         <glance_url>:9292/v2/images/<image_id>
* see 1 location listed in the output, e.g.::

        ..., "locations": [{"url": "something", "metadata": {}}], ...

Test #2 - add 2nd location (localFS)
===============================================================================
* create a copy of the image on the server that runs Glance, for example in
  ``/home/user/mycopy.img``
* use the PATCH API call to add the location::

        $ curl -i -X PATCH 
         -H 'Content-Type: application/openstack-images-v2.1-json-patch'
         -H "X-Auth-Token: $AUTH" http://localhost:9292/v2/images/<image_id> 
         -d '[{"op": "add", "path": "/locations/-", "value": 
            {"url": "file:///home/user/mycopy.img", "metadata": {}}}]'
* use ``glance image-show`` with v2 again, there should be 2 locations now
* use curl to show the locations as in test #1, there should be 2 now
* use glance client to download the image with both v1 and v2

Test #3 (negative) - add non-existent location
===============================================================================
* use the PATCH call as before to add a location that doesn't exist,
  for example ``file:///home/user/nonexistent.img``
* the call should fail
* use ``glance image-show`` again to check if the locations are unchanged

Test #4 - add 3rd location (Swift) 
===============================================================================
* **Note:** this test case can be repeated for any backend you care to test,
  for example HTTP links, Cinder,...
* upload some image into Swift::

        $ swift upload <container> <image>
* get the Swift path to the object, should be something like::

        AUTH_12345/<container>/<image>

  The ``AUTH_12345`` is your user's hash, you can find it on the first line
  of::

        $ swift stat <container>
* add it as a location::

        $ curl -i -X PATCH 
         -H 'Content-Type: application/openstack-images-v2.1-json-patch'
         -H "X-Auth-Token: $AUTH" http://localhost:9292/v2/images/<image_id> 
         -d '[{"op": "add", "path": "/locations/-", "value": 
            {"url": "swift://AUTH_12345/<container>/<image>", "metadata": {}}}]'
* use ``glance image-show``, there should be 3 locations now

Test #5 (negative) - add the same location twice
===============================================================================
* **assumption:** run after successfully finished test #4
* repeat part of test case #4 - don't upload the image again, just add the
  location again (using the same URL to the Swift object)
* the PATCH call should fail
* check if the locations didn't change


Test #6 - add location at first index
===============================================================================
* create another copy of the image in the local FS, for example
  ``/home/user/mycopy2.img``
* add it as the first location::

        $ curl -i -X PATCH 
         -H 'Content-Type: application/openstack-images-v2.1-json-patch'
         -H "X-Auth-Token: $AUTH" http://localhost:9292/v2/images/<image_id> 
         -d '[{"op": "add", "path": "/locations/1", "value": 
            {"url": "file:///home/user/mycopy2.img", "metadata": {}}}]'
* use ``glance image-show``, the new location should be the first and the older
  ones should be in the same order as before (only pushed downwards by one
  position)

Test #7 - add location in-between other locations
===============================================================================
* **assumption:** the image already has at least 2 locations
* similar to test #6, create another copy of the image and add it's location
  the same way, only replace ``"path": "/locations/1"`` with ``"path":
  "/locations/2"``
* use ``glance image-show``, the new location should be second and the old
  locations should have the same order (but positions 3 to N should be pushed
  down by 1)


Test #8 - add location off-index
===============================================================================
* **assumption:** the image has a lot less locations than 50
* similar to test #7, add a new location, but use ``"path": "/locations/50"``
* the location should be added to the bottom of the list of locations

Test #9 - remove location
===============================================================================
* **assumption:** image already has at least 2 locations
* remove the 2nd location::

        $ curl -i -X PATCH 
         -H 'Content-Type: application/openstack-images-v2.1-json-patch'
         -H "X-Auth-Token: $AUTH" http://localhost:9292/v2/images/<image_id> 
         -d '[{"op": "remove", "path": "/locations/2"}]'
* check using ``glance image-show`` that it is gone and the other locations are
  intact and in order

Test #10 - remove last location
===============================================================================
* **assumption:** the image has only one location
* remove first location::

        $ curl -i -X PATCH 
         -H 'Content-Type: application/openstack-images-v2.1-json-patch'
         -H "X-Auth-Token: $AUTH" http://localhost:9292/v2/images/<image_id> 
         -d '[{"op": "remove", "path": "/locations/1"}]'
* (behaviour unspecified) either fail or remove the image completely

Test #11 - replace locations
===============================================================================
* **assumption:** the image already has at least 2 locations
* create another 2 copies of an image, one in local FS and the other in Swift
* replace the current contents of "locations" with new values::

        $ curl -i -X PATCH 
         -H 'Content-Type: application/openstack-images-v2.1-json-patch'
         -H "X-Auth-Token: $AUTH" http://localhost:9292/v2/images/<image_id> 
         -d '[{"op": "replace", "path": "/locations",
              "value": [{"url": "file:///home/user/mycopy3.img", "metadata": {}},
                        {"url": "swift://AUTH_12345/<container>/<image>", "metadata": {}}]}]'
* use ``glance image-show``, the old locations should be gone and only the new
  ones should be there, in the order specified in PATCH
* remove the data in the old locations, for example 
  ``$ rm /home/user/mycopy2.img``
* use glance client to download the image with both v1 and v2

Test #12 - replace locations with an empty value
===============================================================================
* **assumption:** the image already has at least 2 locations
* replace the current contents of "locations" with an empty value::

        $ curl -i -X PATCH 
         -H 'Content-Type: application/openstack-images-v2.1-json-patch'
         -H "X-Auth-Token: $AUTH" http://localhost:9292/v2/images/<image_id> 
         -d '[{"op": "replace", "path": "/locations",
              "value": []'
* it should remove the image completely

Test #13 - add image using v1, add location using v2 at first index
===============================================================================
* upload new image using the Glance API v1
* add a location to it using PATCH, the same way as in test #2, but with
  ``"path": "/locations/1"``
* check if ``glance image-show`` with API v2 shows both locations and the new
  location is first
* use glance client to download the image with both v1 and v2
